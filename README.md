Shibboleth IDP Audit Log Analysis Tool
======================================

> This simple tool started out in [SC-30](https://issues.shibboleth.net/jira/browse/SC-30) back in 2009.
> Any changes and old commits have been replayed and the repository has been reconstructed.

This tool operates on [Shibboleth IDP](https://wiki.shibboleth.net/confluence/display/IDP30/) [audit logs](https://wiki.shibboleth.net/confluence/display/IDP30/AuditLoggingConfiguration) and has the following features:

* Transparently works with compressed logfiles (gzip, bzip2 supported)
* Works as a filter on the command line (use `-` as file name)
* Generates the following statistics from the log files it processes:
  * List of unique Relying Parties (Service Provider entityIDs) accessed
  * Number of unique Relying Parties
  * Number of unique authenticated Principals (i.e, userids)
  * Number of total logins processed
  * Number of events per Relying Party (sorted by number of events)
  * Number of SAML profiles used per relying party, sorted numerically

Requirements
------------

* Python 2.6 or 2.7, or Jython 2.7

Installation
------------

This tool has no external dependencies other than a standard Python runtime. Just make the script executable and copy it into your `$PATH`. E.g.:

```sh
$ chmod +x loganalysis.py && sudo cp loganalysis.py /usr/local/bin/
```

To run this on the JVM (i.e., using [Jython](http://www.jython.org/)) adjust the first line of the script:

```diff
-#!/usr/bin/env python
+#!/usr/bin/env jython
```

and put the Jython shell wrapper script into your `$PATH`. Alternatively replace the whole line with the full path to your Jython wrapper, e.g.:

```diff
-#!/usr/bin/env python
+#!/usr/local/jython-2.7/bin/jython
```

Usage
-----

```sh
$ loganalysis.py --help
Usage: loganalysis.py [options] [files ...]

Options:
  -h, --help            show this help message and exit
  -r, --relyingparties  List of unique relying parties, sorted by name
  -c, --rpcount         Number of unique relying parties
  -u, --users           Number of unique userids
  -l, --logins          Number of logins
  -n, --rploginssort    Number of events per relying party, sorted numerically
  -m, --msgprofiles     Used SAML message profiles per relying party, sorted
  -q, --quiet           Suppress decorative output (or compact JSON)
  -x, --xml             Output stats in XML
  -t, --rrd             Output basic stats for RRD use
  -j, --json            Output stats in JSON
```

More than one option may be used at a time.  
Short and long options may be mixed freely.  
Options may come before or/and after file arguments.

Examples
--------

Show number of SPs accessed, unique users seen, total logins processed, plus usage number for each SP, in descending order:

```sh
$ loganalysis.py -culn /path/to/audit.log
4 unique relying parties
10 unique userids
22 logins

logins   | relyingPartyId
-------------------------
11       | https://foo.example.org
9        | https://bar.example.org/entity
1        | https://wiki.example.net/shibboleth
1        | https://example.org/sp
```

Basic stats plus usage numbers of each SP accessed, in JSON (or try `--xml`):

```sh
$ loganalysis.py --json /path/to/*audit* /other/path/idp-audit* | head
{
  "stats": {
    "logins": 18908,
    "rps": 165,
    "users": 138
  },
  "logins_per_rp": {
    "https://some.example.org/shibboleth-sp": 1,
    "https://my.example.net/shib": 2136,
    "https://yet.another.example.org": 12,
```

Filter JSON output with [jq](https://github.com/stedolan/jq):

```sh
$ loganalysis.py -jq audit.log* | jq .stats.logins
18908
```

Show the top 5 SAML 2.0 SSO services accessed per year from 2011 to 2017:

```sh
$ for year in `seq 2011 2017`; do echo -e "\n=== Year ${year} ==="; loganalysis.py -m /logs/idp-audit-${year}-* | egrep -A5 'saml2[:/]sso'; done

=== Year 2011 ===
urn:mace:shibboleth:2.0:profiles:saml2:sso
47       | https://sp.example.org/shibboleth
17       | http://example.local/sp
11       | https://annoying.example.org/simplesamlphp/module.php/saml/sp/metadata.php/default-sp
5        | https://test-sp.example.com/entity
1        | https://moodle.example.edu

[...]

=== Year 2017 ===
urn:mace:shibboleth:2.0:profiles:saml2:sso
1649     | https://saml.example.com/dontdothis
1244     | https://i-am-an.example.net/entity
1091     | https://sp.example.org/shibboleth
220      | https://but.wait.theres.more.example.org/saml-sp
163      | urn:mace:example.org:has:been:around:for:a:long:time
```

Contributors
------------

* Leif Johansson (XML output)
* Jehan Procaccia (RRD output)
